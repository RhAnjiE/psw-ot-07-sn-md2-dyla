import numpy

from time import time


def naive_relu(x):
    assert len(x.shape) == 2

    x = x.copy()
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            x[i, j] = max(x[i, j], 0)

    return x


def naive_add(x, y):
    assert len(x.shape) == 2
    assert x.shape == y.shape

    x = x.copy()
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            x[i, j] += y[i, j]

    return x


def naive_add_matrix_and_vector(x, y):
    assert len(x.shape) == 2            # dwuwymiarowy tensor
    assert len(y.shape) == 1            # wektor
    assert x.shape[1] == y.shape[0]

    x = x.copy()

    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            x[i, j] += y[j]

    return x


def naive_vector_dot(x, y):
    assert len(x.shape) == 1
    assert len(y.shape) == 1
    assert x.shape[0] == y.shape[0]

    z = 0.

    for i in range(x.shape[0]):
        z += x[i] * y[i]

    return z


def naive_matrix_vector_dot(x, y):
    assert len(x.shape) == 2
    assert len(y.shape) == 1
    assert x.shape[1] == y.shape[0]

    z = numpy.zeros(x.shape[0])

    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            z[i] += naive_vector_dot(x[i, :], y)  # x[i, j] * y[j]

    return z


def naive_matrix_dot(x, y):
    assert len(x.shape) == 2
    assert len(y.shape) == 2
    assert x.shape[1] == y.shape[0]

    z = numpy.zeros((x.shape[0], y.shape[1]))

    for i in range(x.shape[0]):
        for j in range(y.shape[1]):
            row_x = x[i, :]
            column_y = y[:, j]

            z[i, j] = naive_vector_dot(row_x, column_y)

    return z


def main():
    print("")

    print("    [Zadanie 2 - 09.11.2020 - Rozdzial 2.2 - 2.3 - Testowanie szybkosci transformacji]    ")
    print("------------------------------------------------------------------------------------------")

    test_matrix_1 = numpy.random.randint(-100, 100, (100, 100))
    test_matrix_2 = numpy.random.randint(-100, 100, (100, 100))
    test_vector_1 = numpy.random.randint(-50, 50, 100)
    test_vector_2 = numpy.random.randint(-50, 50, 100)

    number_output = 0
    vector_output = numpy.zeros([100], dtype=int)
    matrix_output = numpy.zeros([100, 100], dtype=int)

    # Start methods testing

    t0 = time()

    matrix_output = numpy.maximum(test_matrix_1, 0.)

    t1 = time()

    matrix_output = naive_relu(test_matrix_1)

    t2 = time()

    print('Czas wykonania wbudowanego relu                                          : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego relu                                 : %f sekund' % (t2 - t1))

    print('------------------------------------------------------------------------------------------')

    t0 = time()

    matrix_output = test_matrix_1 + test_matrix_2

    t1 = time()

    matrix_output = naive_add(test_matrix_1, test_matrix_2)

    t2 = time()

    print('Czas wykonania wbudowanego dodawania                                     : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego dodawania                            : %f sekund' % (t2 - t1))

    # print("Tablica 3 wynosi:\n", test_vector_1)

    print('------------------------------------------------------------------------------------------')

    t0 = time()

    matrix_output = numpy.maximum(test_matrix_1, test_vector_1)

    t1 = time()

    matrix_output = naive_add_matrix_and_vector(test_matrix_1, test_vector_1)

    t2 = time()

    print('Czas wykonania wbudowanego dodawania macierzy i wektora                  : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego dodawania macierzy i wektora         : %f sekund' % (t2 - t1))

    print('------------------------------------------------------------------------------------------')

    t0 = time()

    number_output = numpy.dot(test_vector_1, test_vector_2)

    t1 = time()

    number_output = naive_vector_dot(test_vector_1, test_vector_2)

    t2 = time()

    print('Czas wykonania wbudowanego iloczyna wectorow                             : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego iloczyna wectorow                    : %f sekund' % (t2 - t1))

    print('------------------------------------------------------------------------------------------')

    t0 = time()

    vector_output = numpy.dot(test_matrix_1, test_vector_1)

    t1 = time()

    vector_output = naive_matrix_vector_dot(test_matrix_1, test_vector_1)

    t2 = time()

    print('Czas wykonania wbudowanego iloczyna macierzy i wektora                   : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego iloczyna macierzy i wektora          : %f sekund' % (t2 - t1))

    print('------------------------------------------------------------------------------------------')

    t0 = time()

    matrix_output = numpy.dot(test_matrix_1, test_matrix_2)

    t1 = time()

    matrix_output = naive_matrix_dot(test_matrix_1, test_matrix_2)

    t2 = time()

    print('Czas wykonania wbudowanego iloczyna macierzy                             : %f sekund' % (t1 - t0))
    print('Czas wykonania autorskiego naiwnego iloczyna macierzy                    : %f sekund' % (t2 - t1))

    print('------------------------------------------------------------------------------------------')

    return


main()
