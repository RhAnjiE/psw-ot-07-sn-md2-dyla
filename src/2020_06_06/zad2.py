from keras.datasets import mnist
from keras.utils import to_categorical

from keras import models
from keras import layers


def main():
    print("Zadanie 2 - 06.06.2020 - Rozdzial 2.1")
    print("-------------------------------------\n")

    # Laduje zbior obrazow z bazy MNIST zawierajacych recznie narysowane cyfry od 0 do 9.
    # Sluza one do trenowania modelu i jego testowania, aby sprawdzic dokladnosc i straty.
    # Obrazki zostaja pobrane w formie paczki stad: https://s3.amazonaws.com/img-datasets/mnist.npz
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    # Deklaruje nowa siec, ktora bedziemy uczyc
    network = models.Sequential()

    # Dodaje nowa warstwe(filtr danych) do sieci.
    # Jest to warstwa zawierajaca 512 obrazkow 28x28px
    # Uzywamy aktywacji o nazwie relu.
    network.add(layers.Dense(512, activation='relu', input_shape=(28 * 28,)))

    # Tworzymy 10 elementowa warstwe softmax
    # Informujemy dzieki temu, ze mamy 10 cyfr identyfikujacych obrazy
    # Kazdy z tych wynikow okresla prawdopodobienstwo tego, ze w danym obrazie przedstawiona dana cyfre
    network.add(layers.Dense(10, activation='softmax'))

    # Kompilujemy siec uzywajac jako:
    # - optymalizatora "rmsprop" - jest to sposob modyfikowania sieci na podstawie funkcji straty
    # - funkcje straty "categorical_crossentropy" - jest to wartosc, ktora bedzie minimalizowana w procesie trenowania.
    # - metryke monitorowania accuracy - ustawiamy, ze interesuje nas jedynie dokladnosc
    network.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

    # Zmieniamy ksztalt danych na oczekiwany przez siec:
    # Zamienia macierz obrazow treningowych (60000, 28, 28) z wartosciami od 0 - 255 typu uint8
    # na tablice (60000, 784) zawierajaca znormalizowane wartosci 0.0 - 1.0 typu float32
    train_images = train_images.reshape((60000, 28 * 28))
    train_images = train_images.astype('float32') / 255

    # Zamienia macierz obrazow do testow (10000, 28, 28) z wartosciami od 0 - 255
    # na tablice (10000, 784) zawierajaca znormalizowane zmiennoprzecinkowe wartosci 0 - 1
    test_images = test_images.reshape((10000, 28 * 28))
    test_images = test_images.astype('float32') / 255

    # Kodujemy etykiety danych za pomoca kategorii
    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)

    # Dopasujemy model do danych treningowych i trenujemy sieć:
    # Wybieramy rowniez 5 epok, ktore sa iloscia iteracji na danych.
    # Im wiecej, tym madrzejsza nasza siec sie stanie, ale zajmie to rowniez wiecej czasu.
    # Argument batch_size to liczba próbek na aktualizację gradientu.
    network.fit(train_images, train_labels, epochs=5, batch_size=128)

    # Sprawdzamy dokladnosc i wartosc straty przetwarzania
    # testowego zbioru danych pobierajac je odpowiednia metoda
    test_loss, test_acc = network.evaluate(test_images, test_labels)

    # Wypisujemy dokladnosc, ile obrazkow udalo sie nauczyc
    # i odpowiednio przypisac do poprawnych etykiet
    print('Dokladnosc: {}'.format(test_acc))

    # Wypisujemy strate sieci, czyli ile obrazkow
    # nie udalo sie przyporzadkowac do odpowiednich cyfr
    print('Strata: {}'.format(test_loss))

    return


main()
