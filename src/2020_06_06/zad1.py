from keras.datasets import mnist


def main():
    print("Zadanie 1 - 06.06.2020 - Rozdzial 2.1")
    print("-------------------------------------\n")

    # Laduje zbior obrazow z bazy MNIST zawierajacych recznie narysowane cyfry od 0 do 9.
    # Sluza one do trenowania modelu i jego testowania, aby sprawdzic dokladnosc i straty.
    # Obrazki zostaja pobrane w formie paczki stad: https://s3.amazonaws.com/img-datasets/mnist.npz
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    # Wypisujemy dane pobrane z bazy MNIST:
    # Macierz z rozmiarem pobranych obrazow treningowych
    print("Shape  : {}".format(train_images.shape))

    # Ilosc pobranych obrazkow treningowych
    print("Length : {}".format(len(train_labels)))

    # Etykiety obrazow treningowych, czyli w tym wypadku identyfikatory cyfr,
    # ktore zostaly recznie narysowane na tych obrazkach
    print("Labels : {}".format(train_labels))

    # Macierz z rozmiarem pobranych obrazow do testow modelu
    print("Shape  : {}".format(test_images.shape))

    # Ilosc pobranych obrazkow do testow modelu
    print("Length : {}".format(len(test_labels)))

    # Etykiety obrazow do testow modelu, czyli w tym wypadku
    # identyfikatory cyfr, ktore zostaly recznie narysowane na tych obrazkach
    print("Labels : {}".format(test_labels))

    return


main()
